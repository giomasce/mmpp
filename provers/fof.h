#pragma once

#include <iostream>

#include <giolib/memory.h>
#include <giolib/inheritance.h>
#include <giolib/containers.h>

namespace gio::mmpp::provers::fof {

class FOT;
class FOF;
class Variable;
class Functor;

typedef std::shared_ptr<const FOT> term;
typedef std::shared_ptr<const FOF> formula;

struct fot_inheritance {
    typedef FOT base;
    typedef boost::mpl::vector<Variable, Functor> subtypes;
};

struct fot_cmp {
    bool operator()(const FOT &x, const FOT &y) const {
        return compare_base<fot_inheritance>(x, y);
    }
};

class FOT : public inheritance_base<fot_inheritance> {
public:
    virtual void print_to(std::ostream &s) const = 0;
    virtual bool has_free_var(const std::string &name) const = 0;
    virtual term replace(const std::string &var_name, const term &replacement) const = 0;
    std::pair<std::set<std::string>, std::map<std::string, size_t>> collect_vars_functs() const;
    virtual void collect_vars_functs(std::pair<std::set<std::string>, std::map<std::string, size_t>> &vars_functs) const = 0;

protected:
    FOT() = default;
};

template<typename T, typename... Args>
class ObjectCache : public gio::virtual_enable_create<T> {
public:
    static std::shared_ptr<T> create_from_cache(Args&&... args) {
        std::unique_lock lock(ObjectCache::cache_mutex);
        auto range = ObjectCache::cache.equal_range(std::forward_as_tuple(args...));
        if (range.first == range.second) {
            lock.unlock();
            auto ptr = gio::virtual_enable_create<T>::create(std::forward<Args>(args)...);
            lock.lock();
            range.first = ObjectCache::cache.insert(range.first, std::pair(std::forward_as_tuple(args...), std::move(ptr)));
            if ((false)) {
                std::cerr << "Creating new object " << range.first->second << ": ";
                range.first->second->print_to(std::cerr);
                std::cerr << std::endl;
            }
        } else {
            if ((false)) {
                std::cerr << "Retrieving from cache object " << range.first->second << ": ";
                range.first->second->print_to(std::cerr);
                std::cerr << std::endl;
            }
        }
        return range.first->second;
    }

private:
    static inline std::map<std::tuple<Args...>, std::shared_ptr<T>> cache;
    static inline std::mutex cache_mutex;
};

class Functor : public FOT, public ObjectCache<Functor, std::string, std::vector<term>>, public inheritance_impl<Functor, fot_inheritance> {
public:
    void print_to(std::ostream &s) const override;
    bool has_free_var(const std::string &name) const override;
    term replace(const std::string &var_name, const term &replacement) const override;
    void collect_vars_functs(std::pair<std::set<std::string>, std::map<std::string, size_t>> &vars_functs) const override;
    const std::string &get_name() const;
    const std::vector<term> &get_args() const;
    static bool compare(const Functor &x, const Functor &y);

protected:
    Functor(const std::string &name, const std::vector<term> &args);

private:
    std::string name;
    std::vector<term> args;
};

class Variable : public FOT, public ObjectCache<Variable, std::string>, public inheritance_impl<Variable, fot_inheritance> {
public:
    void print_to(std::ostream &s) const override;
    bool has_free_var(const std::string &name) const override;
    term replace(const std::string &var_name, const term &replacement) const override;
    void collect_vars_functs(std::pair<std::set<std::string>, std::map<std::string, size_t>> &vars_functs) const override;
    const std::string &get_name() const;
    static bool compare(const Variable &x, const Variable &y);

protected:
    Variable(const std::string &name);

private:
    std::string name;
};

class FOF;
class Predicate;
class True;
class False;
class Equal;
class Distinct;
class And;
class Or;
class Iff;
class Not;
class Xor;
class Implies;
class Oeq;
class Forall;
class Exists;

struct fof_inheritance {
    typedef FOF base;
    typedef boost::mpl::vector<Predicate, True, False, Equal, Distinct, And, Or, Iff, Not, Xor, Implies, Oeq, Forall, Exists> subtypes;
};

struct fof_cmp {
    bool operator()(const FOF &x, const FOF &y) const {
        return compare_base<fof_inheritance>(x, y);
    }
};

class FOF : public inheritance_base<fof_inheritance> {
public:
    virtual void print_to(std::ostream &s) const = 0;
    virtual bool has_free_var(const std::string &name) const = 0;
    virtual formula replace(const std::string &var_name, const term &replacement) const = 0;
    std::tuple<std::set<std::string>, std::map<std::string, size_t>, std::map<std::string, size_t>> collect_vars_functs_preds() const;
    virtual void collect_vars_functs_preds(std::tuple<std::set<std::string>, std::map<std::string, size_t>, std::map<std::string, size_t>> &vars_functs_preds) const = 0;

protected:
    FOF() = default;
};

class Predicate : public FOF, public ObjectCache<Predicate, std::string, std::vector<term>>, public inheritance_impl<Predicate, fof_inheritance> {
public:
    void print_to(std::ostream &s) const override;
    bool has_free_var(const std::string &name) const override;
    formula replace(const std::string &var_name, const term &replacement) const override;
    void collect_vars_functs_preds(std::tuple<std::set<std::string>, std::map<std::string, size_t>, std::map<std::string, size_t>> &vars_functs_preds) const override;
    const std::string &get_name() const;
    const std::vector<term> &get_args() const;
    static bool compare(const Predicate &x, const Predicate &y);

protected:
    Predicate(const std::string &name, const std::vector<term> &args);

private:
    std::string name;
    std::vector<term> args;
};

class True : public FOF, public ObjectCache<True>, public inheritance_impl<True, fof_inheritance> {
public:
    void print_to(std::ostream &s) const override;
    bool has_free_var(const std::string &name) const override;
    formula replace(const std::string &var_name, const term &replacement) const override;
    void collect_vars_functs_preds(std::tuple<std::set<std::string>, std::map<std::string, size_t>, std::map<std::string, size_t>> &vars_functs_preds) const override;
    static bool compare(const True &x, const True &y);

protected:
    True();
};

class False : public FOF, public ObjectCache<False>, public inheritance_impl<False, fof_inheritance> {
public:
    void print_to(std::ostream &s) const override;
    bool has_free_var(const std::string &name) const override;
    formula replace(const std::string &var_name, const term &replacement) const override;
    void collect_vars_functs_preds(std::tuple<std::set<std::string>, std::map<std::string, size_t>, std::map<std::string, size_t>> &vars_functs_preds) const override;
    static bool compare(const False &x, const False &y);

protected:
    False();
};

class Equal : public FOF, public ObjectCache<Equal, term, term>, public inheritance_impl<Equal, fof_inheritance> {
public:
    void print_to(std::ostream &s) const override;
    bool has_free_var(const std::string &name) const override;
    formula replace(const std::string &var_name, const term &replacement) const override;
    void collect_vars_functs_preds(std::tuple<std::set<std::string>, std::map<std::string, size_t>, std::map<std::string, size_t>> &vars_functs_preds) const override;
    const term &get_left() const;
    const term &get_right() const;
    static bool compare(const Equal &x, const Equal &y);

protected:
    Equal(const term &x, const term &y);

private:
    term left;
    term right;
};

class Distinct : public FOF, public ObjectCache<Distinct, term, term>, public inheritance_impl<Distinct, fof_inheritance> {
public:
    void print_to(std::ostream &s) const override;
    bool has_free_var(const std::string &name) const override;
    formula replace(const std::string &var_name, const term &replacement) const override;
    void collect_vars_functs_preds(std::tuple<std::set<std::string>, std::map<std::string, size_t>, std::map<std::string, size_t>> &vars_functs_preds) const override;
    static bool compare(const Distinct &x, const Distinct &y);

protected:
    Distinct(const term &x, const term &y);

private:
    term left;
    term right;
};

template<typename T>
class FOF2 : public FOF, public virtual_enable_shared_from_this<FOF2<T>> {
public:
    void print_to(std::ostream &s) const override;
    bool has_free_var(const std::string &name) const override;
    formula replace(const std::string &var_name, const term &replacement) const override;
    void collect_vars_functs_preds(std::tuple<std::set<std::string>, std::map<std::string, size_t>, std::map<std::string, size_t>> &vars_functs_preds) const override;
    const formula &get_left() const;
    const formula &get_right() const;
    static bool compare(const T &x, const T &y);

protected:
    FOF2(const formula &left, const formula &right);
    virtual const std::string &get_symbol() const = 0;

    formula left;
    formula right;
};

class And : public FOF2<And>, public ObjectCache<And, formula, formula>, public inheritance_impl<And, fof_inheritance> {
    using FOF2::FOF2;
protected:
    virtual const std::string &get_symbol() const override final;
};

class Or : public FOF2<Or>, public ObjectCache<Or, formula, formula>, public inheritance_impl<Or, fof_inheritance> {
    using FOF2::FOF2;
protected:
    virtual const std::string &get_symbol() const override final;
};

class Iff : public FOF2<Iff>, public ObjectCache<Iff, formula, formula>, public inheritance_impl<Iff, fof_inheritance> {
    using FOF2::FOF2;
protected:
    virtual const std::string &get_symbol() const override final;
};

class Xor : public FOF2<Xor>, public ObjectCache<Xor, formula, formula>, public inheritance_impl<Xor, fof_inheritance> {
    using FOF2::FOF2;
protected:
    virtual const std::string &get_symbol() const override final;
};

class Not : public FOF, public ObjectCache<Not, formula>, public inheritance_impl<Not, fof_inheritance> {
public:
    void print_to(std::ostream &s) const override;
    bool has_free_var(const std::string &name) const override;
    formula replace(const std::string &var_name, const term &replacement) const override;
    void collect_vars_functs_preds(std::tuple<std::set<std::string>, std::map<std::string, size_t>, std::map<std::string, size_t>> &vars_functs_preds) const override;
    const formula &get_arg() const;
    static bool compare(const Not &x, const Not &y);

protected:
    Not(const formula &arg);

private:
    formula arg;
};

class Implies : public FOF2<Implies>, public ObjectCache<Implies, formula, formula>, public inheritance_impl<Implies, fof_inheritance> {
    using FOF2::FOF2;
protected:
    virtual const std::string &get_symbol() const override final;
};

class Oeq : public FOF2<Oeq>, public ObjectCache<Oeq, formula, formula>, public inheritance_impl<Oeq, fof_inheritance> {
    using FOF2::FOF2;
protected:
    virtual const std::string &get_symbol() const override final;
};

class Forall : public FOF, public ObjectCache<Forall, std::shared_ptr<const Variable>, formula>, public inheritance_impl<Forall, fof_inheritance> {
public:
    void print_to(std::ostream &s) const override;
    bool has_free_var(const std::string &name) const override;
    formula replace(const std::string &var_name, const term &replacement) const override;
    void collect_vars_functs_preds(std::tuple<std::set<std::string>, std::map<std::string, size_t>, std::map<std::string, size_t>> &vars_functs_preds) const override;
    const std::shared_ptr<const Variable> &get_var() const;
    const formula &get_arg() const;
    static bool compare(const Forall &x, const Forall &y);

protected:
    Forall(const std::shared_ptr<const Variable> &var, const formula &x);

private:
    std::shared_ptr<const Variable> var;
    formula arg;
};

class Exists : public FOF, public ObjectCache<Exists, std::shared_ptr<const Variable>, formula>, public inheritance_impl<Exists, fof_inheritance> {
public:
    void print_to(std::ostream &s) const override;
    bool has_free_var(const std::string &name) const override;
    formula replace(const std::string &var_name, const term &replacement) const override;
    void collect_vars_functs_preds(std::tuple<std::set<std::string>, std::map<std::string, size_t>, std::map<std::string, size_t>> &vars_functs_preds) const override;
    const std::shared_ptr<const Variable> &get_var() const;
    const formula &get_arg() const;
    static bool compare(const Exists &x, const Exists &y);

protected:
    Exists(const std::shared_ptr<const Variable> &var, const formula &x);

private:
    std::shared_ptr<const Variable> var;
    formula arg;
};

extern template class FOF2<And>;
extern template class FOF2<Or>;
extern template class FOF2<Iff>;
extern template class FOF2<Xor>;
extern template class FOF2<Implies>;
extern template class FOF2<Oeq>;

}
