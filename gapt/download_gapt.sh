#!/bin/bash

set -e

wget -q --continue https://www.logic.at/gapt/downloads/gapt-2.16.0.tar.gz
tar zxf gapt-2.16.0.tar.gz
ln gapt-2.16.0/gapt-2.16.0.jar
